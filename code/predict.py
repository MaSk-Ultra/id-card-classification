import matplotlib.pyplot as plt
import skimage as s
from keras.preprocessing.image import load_img, array_to_img, img_to_array
import os
import numpy as np
import seaborn as sns
import pandas as pd
import keras
import argparse
import cv2
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Dense, Activation, BatchNormalization
import tensorflow as tf
import math
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
from sklearn.model_selection import train_test_split

parser = argparse.ArgumentParser()
parser.add_argument('--path', type=str, required = True)
args = vars(parser.parse_args())

json_file = open('../artifacts/model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
model = model_from_json(loaded_model_json)
# load weights into new model
model.load_weights("../artifacts/model.h5")
def load_img_shapes(path_to_img):
    return cv2.imread(path_to_img).shape

def load_img(path_to_img):
    return cv2.imread(path_to_img)


img_path = args["path"]
img = image.load_img(img_path, target_size=(192, 192))
img = image.img_to_array(img)[:,:,0]                    # (height, width, channels)
img = np.expand_dims(img, axis=0)         # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
img /= 255.


predictions = model.predict_classes(img)

print(predictions)