import matplotlib.pyplot as plt
import skimage as s
from keras.preprocessing.image import load_img, array_to_img, img_to_array
import os
import numpy as np
import seaborn as sns
import pandas as pd
import keras
import cv2
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Dense, Activation, BatchNormalization
import tensorflow as tf
import math
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
from sklearn.model_selection import train_test_split

data_dir = '../data'
labels_csv = os.path.join(data_dir, 'gicsd_labels.csv')
images_dir = os.path.join(data_dir, 'images')
data_ids = [path for path in os.listdir(images_dir)]
data_df = pd.read_csv(labels_csv)
data_df.columns = ['IMAGE_FILENAME', 'LABEL']
def rule(x):
    if x['LABEL'].strip() == 'NO_VISIBILITY':
        return '2'
    if x['LABEL'].strip() == 'PARTIAL_VISIBILITY':
        return '1'
    if x['LABEL'].strip() == 'FULL_VISIBILITY':
        return '0'
    
    return np.nan

data_df['LABEL'] = data_df.apply(rule, axis = 1)
class DataGenerator(tf.compat.v2.keras.utils.Sequence):

    def __init__(self, list_IDs, labels, image_path,
                 to_fit=True, batch_size=32, dim=(192, 192),
                 n_channels=3, n_classes=10, shuffle=False):
        self.list_IDs = list_IDs
        self.labels = labels
        self.image_path = image_path
        self.n_channels = n_channels
        self.to_fit = to_fit
        self.n_classes = n_classes
        self.dim = dim
        self.shuffle = shuffle
        self.batch_size = batch_size
        self.n = 0
        self.on_epoch_end()

    def __next__(self):
        # Get one batch of data
        data = self.__getitem__(self.n)
        # Batch index
        self.n += 1

        # If we have processed the entire dataset then
        if self.n >= self.__len__():
            self.on_epoch_end
            self.n = 0

        return data
    
    def __len__(self):
        # Return the number of batches of the dataset
        return math.ceil(len(self.indexes)/self.batch_size)
    
    def __getitem__(self, index):
    
        # Generate indexes of the batch
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X = self._generate_x(list_IDs_temp)

        if self.to_fit:
            y = self._generate_y(indexes)
            return X, y
        else:
            return X
    
    def on_epoch_end(self):
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)
        
    def _generate_x(self, list_IDs_temp):
        # Initialization
        X = np.empty((self.batch_size, *self.dim, 1))

        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store sample
            X[i,] = self._load_blue_channel_image(os.path.join(self.image_path, ID))

        return X
    
    def _generate_y(self, indexes):

        y = np.empty((self.batch_size, 1), dtype=int)

        # Generate labels
        for i,idx in enumerate(indexes):
            # Store sample
            temp = self.labels[idx]
            if temp.strip() == '2':
                z = 2
            if temp.strip() == '1':
                z = 1
            if temp.strip() == '0':
                z = 0
            y[i,] = z

        return keras.utils.to_categorical(y, num_classes = self.n_classes)
    
    def _load_blue_channel_image(self, image_path):
        img = cv2.imread(image_path)
        img = img[:,:,0].reshape(192,192,1)
        img = img / 255
        return img

data_idx = data_df['IMAGE_FILENAME'].tolist()
labels_idx = data_df['LABEL'].tolist()

X_train, X_test, y_train, y_test = train_test_split(data_idx, labels_idx, test_size=0.2, random_state=42)

train_generator = DataGenerator(X_train, y_train,image_path = images_dir, n_classes = 3)
val_generator = DataGenerator(X_test, y_test,image_path = images_dir, n_classes = 3)

model = Sequential()

model.add(Conv2D(32, (3, 3), activation='relu', input_shape=(192, 192, 1)))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(128, (3, 3), activation='relu'))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(512, activation='relu'))
model.add(BatchNormalization())
model.add(Dropout(0.5))
model.add(Dense(3, activation='softmax')) # 2 because we have cat and dog classes

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

model.summary()

earlystop = EarlyStopping(patience=10)
learning_rate_reduction = ReduceLROnPlateau(monitor='val_acc', 
                                            patience=2, 
                                            verbose=1, 
                                            factor=0.5, 
                                            min_lr=0.00001)
callbacks = [earlystop, learning_rate_reduction]

epochs=150
batch_size = 32
steps_per_epoch = len(train_generator)
validation_steps = len(val_generator)
history = model.fit_generator(
    train_generator,
    epochs=epochs,
    steps_per_epoch=steps_per_epoch,
    validation_data=val_generator,
    validation_steps=validation_steps,
    callbacks=callbacks
)

model_json = model.to_json()
with open("../artifacts/model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("../artifacts/model.h5")
